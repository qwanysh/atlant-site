from django.apps import AppConfig


class ClientsConfig(AppConfig):
    name = 'clients'
    default_auto_field = 'django.db.models.AutoField'
