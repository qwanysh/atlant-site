from django.contrib import admin

from clients.models import ClientRequest, Manager


@admin.register(ClientRequest)
class ClientRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'phone_number', 'tag', 'created_at']
    ordering = ['-id']


@admin.register(Manager)
class ManagerAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'is_active']
    list_editable = ['is_active']
