# atlant-site

### Setup
```bash
cp .env.example .env
# configure .env
pipenv install --dev
pipenv run python manage.py migrate
pipenv run python manage.py createsuperuser
```

### Build frontend
```bash
npm i
npm run build
# or
npm run dev
```

### Run
```bash
pipenv run python manage.py runserver
```

### Test
```bash
pipenv run pytest
```
