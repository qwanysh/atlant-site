$(function () {
  const inputs = $('.phone-mask');
  const options = {
    mask: '+{0} (000) 000-00-00',
  };

  inputs.each(function () {
    IMask(this, options);
  });
});
