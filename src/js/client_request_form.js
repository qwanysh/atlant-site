$(function () {
  const formToObject = function (form) {
    const formArray = form.serializeArray();
    const resultObj = {};

    for (const item of formArray) {
      resultObj[item.name] = item.value;
    }

    return resultObj;
  };

  const forms = $('.client-request-form');

  forms.each(function () {
    $(this).validate({
      errorClass: 'form__input--error',
      rules: {
        name: 'required',
        phone_number: {
          required: true,
          minlength: 18,
        },
      },
      errorPlacement: function () {},
      submitHandler: function (form) {
        form = $(form);
        const submitButton = form.find('button');
        submitButton.prop('disabled', true);

        $.ajax({
          url: form.attr('action'),
          method: 'POST',
          data: formToObject(form),
          success() {
            showModal(
              [
                'Ваша заявка отправлена!',
                'Пока наш менеджер набирает Ваш номер, можете посетить наш Instagram',
              ],
              `
                <a
                  class="response-modal__button"
                  href="https://instagram.com/massage_atlant"
                  target="_blank"
                >перейти в instagram</a>
              `,
            );
            form.trigger('reset');
          },
          error(err) {
            showModal([err.statusText]);
          },
          complete() {
            submitButton.prop('disabled', false);
          },
        });
      },
    });
  });
});
