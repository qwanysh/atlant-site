$(function () {
  let lastScrollTop = $(window).scrollTop();
  const button = $('.mobile-buttons__button--scroll-top');

  window.addEventListener('scroll', () => {
    const scrollTop = $(window).scrollTop();

    if (scrollTop === 0 || lastScrollTop - scrollTop < 0) {
      button.addClass('mobile-buttons__button--hidden');
    } else if (lastScrollTop - scrollTop > 0) {
      button.removeClass('mobile-buttons__button--hidden');
    }

    lastScrollTop = scrollTop;
  });
});
