$(function () {
  const button = $('.cta-button');
  const input = $('#cta-input');

  button.on('click', function () {
    setTimeout(() => input.focus(), 1000);
  });
});
