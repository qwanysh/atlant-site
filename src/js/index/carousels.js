$(function () {
  const defaultOptions = {
    classes: {
      arrows: 'carousel__arrows',
      arrow: 'carousel__arrow',
      prev: 'carousel__arrow--prev',
      next: 'carousel__arrow--next',
      pagination: 'carousel__pagination',
      page: 'carousel__pagination-item',
    },
    type: 'loop',
  };

  new Splide(document.getElementById('testimonials-carousel'), {
    ...defaultOptions,
    perPage: 3,
    focus: 'center',
    autoplay: true,
    breakpoints: {
      992: {
        perPage: 2,
      },
      576: {
        perPage: 1,
      },
    },
  }).mount();

  new Splide(document.getElementById('team-carousel'), {
    ...defaultOptions,
    perPage: 4,
    autoplay: true,
    breakpoints: {
      992: {
        perPage: 3,
      },
      768: {
        perPage: 2,
        focus: 'center',
      },
      576: {
        perPage: 1.5,
      },
    },
  }).mount();
});
