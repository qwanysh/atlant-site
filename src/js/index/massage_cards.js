$(function () {
  const INITIAL_COUNT = 6;
  const NEXT_COUNT = 3;

  const button = $('#massage-cards-button');
  const container = $('#massage-cards-container');
  const cards = container.find('.services__column');

  const total = cards.length;
  let currentCount = INITIAL_COUNT;

  const updateCards = function () {
    $.each(cards, function (index) {
      if (index < currentCount) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });

    if (currentCount >= total) {
      button.remove();
    } else {
      currentCount += NEXT_COUNT;
    }
  };

  updateCards();

  button.on('click', function (event) {
    event.preventDefault();

    updateCards();
  });
});
