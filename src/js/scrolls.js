$(function () {
  const moveTo = new MoveTo({ duration: 1000 });
  const triggers = $('.move-to');

  triggers.each(function () {
    moveTo.registerTrigger(this);
  });
});
