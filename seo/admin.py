from django.contrib import admin

from seo.models import Page


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ['path', 'description']
