from django.db import models


class Page(models.Model):
    path = models.CharField(verbose_name='Путь', max_length=100)
    description = models.TextField(verbose_name='Описание')

    def __str__(self):
        return self.path
