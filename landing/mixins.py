from landing.models import Discount


class MenuItemsMixin:
    menu_items = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu_items'] = self.menu_items
        return context


class CountdownMixin:
    countdown_target = None

    def get_context_data(self, **kwargs):
        if not self.countdown_target:
            raise NotImplementedError('countdown_target is not set')

        context = super().get_context_data(**kwargs)

        try:
            discount = Discount.objects.get(target=self.countdown_target)
            context['discount'] = discount
        except Discount.DoesNotExist:
            pass

        return context
