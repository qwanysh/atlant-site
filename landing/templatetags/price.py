from django import template

register = template.Library()


@register.filter
def beautify_price(price):
    digits = list(str(price))
    counter = 0
    beautified_price = ''

    for index in range(len(digits) - 1, -1, -1):
        if counter == 3:
            beautified_price += ' '
            counter = 0

        beautified_price += digits[index]
        counter += 1

    return beautified_price[::-1]
