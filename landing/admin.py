from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django.contrib import admin
from django.utils.html import format_html

from landing.forms import CourseTestimonialAdminForm, TestimonialAdminForm
from landing.models import (Course, CourseTestimonial, Discount, Massage,
                            MassageResult, Specialist, Testimonial)


class MassageResultInline(SortableInlineAdminMixin, admin.StackedInline):
    model = MassageResult


@admin.register(Massage)
class MassageAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['name', 'old_price', 'price', 'is_active', 'page_link']
    list_editable = ['old_price', 'price', 'is_active']
    inlines = [MassageResultInline]

    def page_link(self, instance):
        return format_html(
            '<a href="{url}" target="_blank">Перейти</a>',
            url=instance.get_page_url(),
        )

    page_link.short_description = 'Страница'


@admin.register(Testimonial)
class TestimonialAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['id', 'image', 'video_url']
    form = TestimonialAdminForm


@admin.register(Specialist)
class SpecialistAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['first_name', 'last_name']


@admin.register(Discount)
class DiscountAdmin(admin.ModelAdmin):
    list_display = ['target', 'percent', 'deadline']
    list_editable = ['percent', 'deadline']


@admin.register(Course)
class CourseAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['name', 'old_price', 'price', 'is_active']
    list_editable = ['old_price', 'price', 'is_active']


@admin.register(CourseTestimonial)
class CourseTestimonialAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['id', 'image', 'video_url']
    form = CourseTestimonialAdminForm
