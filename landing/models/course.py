from django.db import models


class Course(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)
    position = models.PositiveIntegerField(verbose_name='Позиция', default=0)
    old_price = models.PositiveIntegerField(
        verbose_name='Стоимость без скидки',
    )
    price = models.PositiveIntegerField(verbose_name='Стоимость')
    description = models.TextField(verbose_name='Описание')
    is_active = models.BooleanField(verbose_name='Активен', default=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['position']
