from django.db import models
from django.utils import timezone


class Discount(models.Model):
    class Target(models.TextChoices):
        MASSAGES = 'massages', 'Массажи'
        COURSES = 'courses', 'Курсы'
        CERTIFICATES = 'certificates', 'Сертификаты'

    target = models.CharField(
        verbose_name='Скидка действует на', max_length=100, unique=True,
        choices=Target.choices, default=Target.MASSAGES,
    )
    percent = models.PositiveIntegerField(
        verbose_name='Процент скидки', default=0,
    )
    deadline = models.DateTimeField(verbose_name='Дата и время окончания')

    @property
    def seconds_left(self):
        now = timezone.now()

        if now <= self.deadline:
            return (self.deadline - now).total_seconds()

    def __str__(self):
        return self.target
