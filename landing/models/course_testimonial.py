from django.db import models

from .testimonial import Testimonial


class CourseTestimonialManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            related_with=Testimonial.RelatedWith.COURSES,
        )


class CourseTestimonial(Testimonial):
    objects = CourseTestimonialManager()

    class Meta:
        proxy = True
