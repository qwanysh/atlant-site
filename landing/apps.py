from django.apps import AppConfig


class LandingConfig(AppConfig):
    name = 'landing'
    default_auto_field = 'django.db.models.AutoField'
