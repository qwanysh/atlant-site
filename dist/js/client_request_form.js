$(function(){const t=$(".client-request-form");t.each(function(){$(this).validate({errorClass:"form__input--error",rules:{name:"required",phone_number:{required:!0,minlength:18}},errorPlacement:function(){},submitHandler:function(t){const e=(t=$(t)).find("button");e.prop("disabled",!0),$.ajax({url:t.attr("action"),method:"POST",data:function(t){const e={};for(const r of t.serializeArray())e[r.name]=r.value;return e}(t),success(){showModal(["Ваша заявка отправлена!","Пока наш менеджер набирает Ваш номер, можете посетить наш Instagram"],`
                <a
                  class="response-modal__button"
                  href="https://instagram.com/massage_atlant"
                  target="_blank"
                >перейти в instagram</a>
              `),t.trigger("reset")},error(t){showModal([t.statusText])},complete(){e.prop("disabled",!1)}})}})})});