FROM python:3.8-slim
RUN useradd --create-home --home-dir /app --shell /bin/bash app
WORKDIR /app
COPY Pipfile Pipfile.lock ./
RUN pip install --upgrade pip && \
    pip install pipenv && \
    pipenv install --system --deploy --ignore-pipfile --dev
USER app
COPY . .
